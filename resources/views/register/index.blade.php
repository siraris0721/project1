@extends('blog.main')

@section('container')

<div class="row justify-content-center">
    <div class="col-lg-4">
        <div class="card bourder-0 shadow rounded mt-4">
            <div class="card-body">

            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
            </div>
            @endif

                <main class="form-signin">
                    <form action="{{ route('store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h1 class="h3 mb-3 fw-normal text-center">Register</h1>

                        <div class="form-floating md-3">
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" value="{{ old('name') }}" placeholder="Name">
                        <label for="name">Name</label>

                        @error('name')
                            <div class="alert alert-danger md-3">
                                {{ $message }}
                            </div>
                        @enderror

                        </div>

                        <div class="form-floating md-3">
                        <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="username" value="{{ old('username') }}" placeholder="Username">
                        <label for="username">Username</label>

                        @error('username')
                            <div class="alert alert-danger md-3">
                                {{ $message }}
                            </div>
                        @enderror

                        </div>

                        <div class="form-floating md-3">
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" placeholder="name@example.com">
                        <label for="email">Email address</label>

                        @error('email')
                            <div class="alert alert-danger md-3">
                                {{ $message }}
                            </div>
                        @enderror

                        </div>

                        <div class="form-floating mb-3">
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" >
                        <label for="password">Password</label>

                        @error('password')
                            <div class="alert alert-danger md-3">
                                {{ $message }}
                            </div>
                        @enderror

                        </div>
                        <button class="w-100 btn btn-lg btn-primary" type="submit">Register</button>
                        <small class="d-block text-center mt-2">Already registered?<a href="/login">Login</a></small>
                    </form>
                </main>
            </div>
        </div>
    </div>
</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

@endsection
