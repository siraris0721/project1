
@extends('blog.main')

@section('container')



<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card bourder-0 rounded">
                <div class="card-body">
                    <h2 style="margin: 30px; text-align: center;">{{ $blog->title }}</h2>
                    <div class="text-center py-5">
                        <img src="{{ Storage::url('public/blogs/').$blog->image }}" class="rounded" style="width: 50%">
                    </div>
                    <div class="mx-5 py-4"><p>{!! $blog->content !!}</p></div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection


