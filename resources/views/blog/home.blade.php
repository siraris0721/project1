@extends('blog.main')

@section('container')
<tbody>

@if($blogs->count())

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card mb-3 py-3">
                <div class="text-center bg-auto" style="padding: 30px">
                    <img src="{{ Storage::url('public/blogs/').$blogs[0]->image }}" class="card-img-top" style="width: 60%;" alt="{{ $blogs[0]->title }}">
                </div>
                <div class="card-body text-center">
                        <h3 class="card-title"><a href="{{ route('blog.show', $blogs[0]->slug) }}" class="text-decoration-none text-dark">{{ $blogs[0]->title }}</a></h3>
                        <p class="card-text">{!! Str::of($blogs[0]->content)->words(30) !!}</p>
                        <p class="card-text"><small class="text-muted">{{ $blogs[0]->created_at->diffForHumans() }}</small></p>
                        <a href="{{ route('blog.show', $blogs[0]->slug) }}" class="btn btn-sm btn-primary ms-auto">Read more</a>
                    </div>
                </div>
            </div>
        </div>
</div>

@else

    <p class="text-center fs-3">No Post found</p>

@endif


<div class="container">
    <div class="row">
        @foreach ($blogs->skip(1) as $blog)
        <div class="col-md-4 mb-3">
                <div class="card text-center">
                    <img src="{{ Storage::url('public/blogs/').$blog->image }}" class="card-img-top" alt="{{ $blog->title }}">
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{ route('blog.show', $blog->slug) }}" class="text-decoration-none text-dark">{{ $blog->title }}</a></h5>
                        <p class="card-text">{!! Str::of($blog->content)->words(20) !!}</p>
                        <p class="card-text"><small class="text-muted">{{ $blog->created_at->diffForHumans() }}</small></p>
                        <a href="{{ route('blog.show', $blog->slug) }}" class="btn btn-sm btn-primary ms-auto">Read more</a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</tbody>

{{ $blogs->links() }}

@endsection
