@extends('blog.main')

@section('container')
@auth
        <div class="container mt5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card bourder-0 shadow rounded">
                        <div class="card-body">
                            <a href="/blog/create" class="btn btn-md btn-success mb-3" title="Nambah ora? masa ora nambah">Add</a>
                            <table class="table table-bordered" style="text-align: center; overflow-y: auto">
                                <thread>
                                    <tr>
                                        <th scope="col">Image</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Content</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thread>
                                <tbody>
                                    @forelse ($blogs as $blog)
                                    <tr>
                                    <td class="text-center">
                                        <img src="{{ Storage::url('public/blogs/').$blog->image }}" class="rounded" style="width: 150px">
                                    </td>
                                    <td>{{ Str::of($blog->title)->title() }}</td>
                                    <td>{!! Str::of($blog->content)->words(30) !!}</td>
                                    <td>
                                        <a href="{{ route('blog.show', $blog->slug) }}" class="badge bg-info"><span data-feather="eye"></span></a>
                                        <form onsubmit="return confirm('Apakah Anda Yakin ?');" action="/blog/{{ $blog->slug }}" method="POST">
                                            <a href="/blog/{{ $blog->slug }}/edit" class="badge bg-warning"><span data-feather="edit"></span></a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="badge bg-danger border-0"><span data-feather="x-circle"></span></button>
                                        </form>
                                    </td>
                                </tr>
                              @empty
                                  <div class="alert alert-danger">
                                      Post hasn't added.
                                  </div>
                              @endforelse
                            </tbody>
                          </table>
                          {{ $blogs->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endauth
@endsection
