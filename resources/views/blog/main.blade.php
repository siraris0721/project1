<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <!-- <link rel="stylesheet" href="/css/style.css"> -->
    <link rel="shortcut icon" href="https://www.pngitem.com/pimgs/m/11-116289_dk-projects-dk-logo-hd-png-download.png" type="image/x-icon">
    <title>dadangKonelo | {{ $title }} </title>
</head>
<body>

<nav class="navbar navbar-light bg-light fixed-top">
  <div class="container-fluid">
    <a class="navbar-brand" href="/"><h2>{{ $title1 }}</h2></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar" aria-controls="offcanvasNavbar">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel">
      <div class="offcanvas-header">
        @auth
        <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Welcome, {{ auth()->user()->name }}</h5>
        @else
        <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Menu</h5>
        @endauth
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
      </div>
      <div class="offcanvas-body">
        <ul class="navbar-nav justify-content-end flex-grow-1 pe-3">
          <li class="nav-item">
            <a class="nav-link {{ ($title === 'Home') ? 'active' : '' }}" aria-current="page" href="/">Home</a>
          </li>
          @auth
          <li class="nav-item">
            <a class="nav-link {{ ($title === 'Dashboard') ? 'active' : '' }}" href="/blog">Dashboard</a>
          </li>
          @endauth
          <li class="nav-item">
            <a class="nav-link {{ ($title === 'About') ? 'active' : '' }}" href="/about">About</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="offcanvasNavbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Account
            </a>
            <ul class="dropdown-menu" aria-labelledby="offcanvasNavbarDropdown">
                @auth
                <li>
                    <form action="{{ route('logout') }}" method="post">
                        @csrf
                        <button type="submit" class="dropdown-item"><i class="bi bi-box-arrow-right"></i>Logout</button>
                    </form>
                </li>
                @else
                <li><a class="dropdown-item" href="/login"><i class="bi bi-box-arrow-in-right"></i> Login</a></li>
                <li><a class="dropdown-item" href="/register"><i class="bi bi-pencil-square"></i> Register</a></li>
              @endauth
            </ul>
          </li>
        </ul>
        <form class="d-flex" action="/blog">
          <input class="form-control me-2" type="text" placeholder="Search" name="Search">
          <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
      </div>
    </div>
  </div>
</nav>

    <div class="row mt-5">
        <div class="row mt-5">
            <@yield('container')
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://unpkg.com/feather-icons"></script>
    <script>
        feather.replace()
        //message with toastr
        @if(session()->has('success'))

            toastr.options =
                    {
                        "closeButton" : true,
                        "progressBar" : true
                    }

            toastr.success('{{ session('success') }}');

        @elseif(session()->has('error'))

            toastr.options =
                    {
                        "closeButton" : true,
                        "progressBar" : true
                    }

            toastr.error('{{ session('error') }}');

        @endif
    </script>
</body>
</html>
