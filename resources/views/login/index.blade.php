@extends('blog.main')

@section('container')

<div class="row justify-content-center">
    <div class="col-lg-4">
        <div class="card bourder-0 shadow rounded mt-5">
            <div class="card-body">

            @if(session()->has('LoginError'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('LoginError') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="close"></button>
            </div>
            @endif

                <main class="form-signin">
                    <form action="{{ route('auth') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <h1 class="h3 mb-4 fw-normal text-center">Login</h1>

                        <div class="form-floating md-3">
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" placeholder="name@example.com" autofocus>
                        <label for="email">Email address</label>

                        @error('email')
                            <div class="alert alert-danger md-3">
                                {{ $message }}
                            </div>
                        @enderror

                        </div>

                        <div class="form-floating mb-3">
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                        <label for="password">Password</label>
                        </div>

                        <button class="w-100 btn btn-lg btn-primary" type="submit">Login</button>

                    </form>
                    <small class="d-block text-center mt-2">Not registered? <a href="/register">register now!</a></small>
                </main>
            </div>
        </div>
    </div>
</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

@endsection
