<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    public function index(){
        return view('register.index', ["title" => "Register", "title1" => "dadangKonelo"]);
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'name' => 'required|min:5|max:255|unique:users',
            'username' => 'required|min:5|max:255|unique:users',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:5|max:255|unique:users',
        ]);

        $validatedData['password'] = Hash::make($validatedData['password']);

        User::create($validatedData);

        if ($validatedData) {
            return redirect()->intended('/login')->with(['success' => 'Register Berhasil']);
        }
        else {
            return redirect()->intended('/login')->with(['error' => 'Register Gagal']);
        }

    }
}
