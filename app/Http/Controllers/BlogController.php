<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\User;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use \Cviebrock\EloquentSluggable\Services\SlugService;

class BlogController extends Controller
{
     /**
     * index
     *
     * @return void
     */

     public function index(Blog $blog, Request $request){
         //$orders = Order::search('search')->latest()->get();
         $blogs = Blog::latest()->paginate(10);
         return view('blog.index',["title" => "Dashboard", "title1" => "Dashboard"], compact('blog', 'blogs'));
     }

    /**
      * home
      *
      * @return void
      */

     public function home(Blog $blog){
         $blogs = Blog::latest()->paginate(10);
         return view('blog.home',["title" => "Home", "title1" => "dadangKonelo"], compact('blog', 'blogs'));
     }

     /**
      * about
      *
      * @return void
      */

      public function about(){
         return view('blog.about', ["title" => "About", "title1" => "About"]);
      }

     /**
      * show
      *
      * @param int $id
      * @return void
      */

     public function show(Blog $blog){
        return view('blog.show', ['blog' => $blog, "title" => "Show", "title1" => "dadangKonelo"], compact('blog'));
     }

     /**
     *create
     *
     * @return void
     */

     public function create(){
         return view('blog.create');
     }

     /**
      *store
      *
      * @param mixed $request
      * @return void
      */

      public function store(Request $request){
          $this->validate($request, [
            'image'     => 'required|image|mimes:png,jpg,jpeg',
            'title'     => 'required',
            'content'   => 'required',
          ]);

          $image = $request->file('image');
          $image->storeAs('public/blogs', $image->hashName());

          $slug = SlugService::createSlug(Blog::class, 'slug', $request->title);

          $blog = Blog::create([
              'image' => $image->hashname(),
              'title' => $request->title,
              'slug' => $slug,
              'content' => $request->content
          ]);

          if ($blog) {
              return redirect()->route('blog.index')->with(['success' => 'Data berhasil disimpan!']);
          }
          else {
              return redirect()->route('blog.index')->with(['error' => 'Data gagal disimpan!']);
          }
      }

      /**
      * edit
      *
      * @param mixed $blog
      * @return void
      */

      public function edit(Blog $blog){

          return view('blog.edit', compact('blog'));
      }

      /**
      * update
      *
      * @param mixed $request
      * @param mixed $blog
      * @return void
      */

      public function update(Request $request, Blog $blog){

        $this->validate($request, [
              'title' => 'required',
              'content' => 'required',
          ]);

          $blog = Blog::findOrFail($blog->id);

          if ($request->file('image') == "") {

            $blog->update([
                'title' => $request->title,
                'slug' => $slug,
                'content' => $request->content,
            ]);

            $blog->save();
        }

            else {

                Storage::disk('local')->delete('public/blog/'.$blog->image);

                $image = $request->file('image');
                $image->storeAs('public/blogs', $image->hashName());

                $slug = SlugService::createSlug(Blog::class, 'slug', $request->title);


                $blog->update([
                  'image' => $image->hashname(),
                  'title' => $request->title,
                  'slug' => $slug,
                  'content' => $request->content,
                ]);

                $blog->save();

            }

            if ($blog) {
                return redirect()->route('blog.index')->with(['success' => 'Data berhasil diupdate!']);
            }
            else {
                return redirect()->route('blog.index')->with(['error' => 'Data gagal diupdate!']);
            }
      }

      /**
      * destroy
      *
      * @param mixed $slug
      * @return void
      */

      public function destroy(Blog $blog){

        Blog::destroy($blog->id);

        if ($blog) {
            return redirect()->route('blog.index')->with(['success' => 'Data berhasil dihapus']);
        }
        else {
            return redirect()->route('blog.index')->with(['error' => 'Data gagal dihapus']);
        }

      }




}
